## Deploy First time
- npm install -g knex
- Pull 
- npm install über https://gist.github.com/SuperPaintman/851b330c08b2363aea1c870f0cc1ea5a
- .env.example umbenennen  in .env und anpassen
- web/config.example.js anpassen und in config.js umbennen
- npm run build
- knex migrate:latest --env production //Füllt die datenbank
- Starten über **server/server.js** mit env variable production kann man in plesk einstellen
- renka.de/install aufrufen den ersten admin user erstellen
- renka.de/admin und ab gehts junge

# Update 
- server stopen
- pull
- dist ordner leeren
- npm run build
- knex migrate:latest --env production
- server/server.js wieder starten