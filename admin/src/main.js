// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import store from './store'
import VueRouter from 'vue-router'
import App from './App'
import authCheck from './utils/auth'
/**
 * Views
 */
import Login from './views/Login'
import PostIndex from './views/Posts/Index.vue'
import PostEdit from './views/Posts/Edit.vue'
import PostCreate from './views/Posts/Create.vue'
import CategoriesIndex from './views/Categories/Index.vue'
import MediaIndex from './views/Media/Index.vue'
import NotFound from './views/NotFound.vue'

//Vue.component('Edit', PostEdit)

Vue.use(VueRouter)
const routes = [
  { path: '/login', name: 'login', component: Login },
  { path: '/posts', name: 'posts', component: PostIndex },
  { path: '/post/create', name: 'post-create', component: PostCreate },
  { path: '/categories', name: 'categories', component: CategoriesIndex },
  { path: '/post/edit/:slug?', component: PostEdit },
  { path: '/media', component: MediaIndex, name: 'media' },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'history',
  base: 'admin',
  routes
})
//Only for the first hit
if (authCheck()) {
  store.dispatch('auth/LOCAL_LOGIN')
}

router.beforeEach((to, from, next) => {
  next()
})

new Vue({
  ...App,
  store,
  router
}).$mount('#app')
