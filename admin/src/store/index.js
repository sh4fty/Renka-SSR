import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/Auth'
import blog from './modules/blog'
import media from './modules/media'

Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    auth,
    media,
    blog
  },
  strict: process.env.NODE_ENV !== 'production',
})

export default store

if (module.hot) {
  /* eslint global-require: 0 */
  module.hot.accept([
    './modules/Auth',
    './modules/blog',
    './modules/media'
  ], () => {
    // swap in the new parts of store
    store.hotUpdate({
      modules: {
        blog: require('./modules/blog').default
      }
    })
  })
}
