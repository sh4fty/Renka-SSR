import * as types from '../mutation-types'

const alertModule = {
  state: {
    show: false,
    message: 'No Message added',
    type: 'success',
    duration: 2000,
  },

  actions: {
    [types.SET_ALERT]({ commit }, payload) {

    }

  },
  mutations: {}
}

export default alertModule;
