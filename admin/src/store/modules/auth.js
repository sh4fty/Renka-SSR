import * as types from '../mutation-types'
import api from '../../utils/api'
import authCheck from '../../utils/auth'
import jwtDecode from 'jwt-decode'
const authModule = {
  state: { user: false },
  actions: {
    async [types.LOGIN] ({ commit }, userData) {
      try {
        var res = await api.post('/login', userData)
        localStorage.setItem('token', res.data.token)

        const decodedToken = jwtDecode(res.data.token)
        commit(types.SET_USER, decodedToken.user)
      } catch (e) {
        console.error(e)
      }
    },

    [types.LOGOUT] ({ commit }) {
      localStorage.removeItem('token')
      commit(types.SET_USER, false)
    },
    [types.LOCAL_LOGIN] ({ commit }) {

      var token = authCheck()
      if (token) {
        commit(types.SET_USER, token.user)
      }
    }
  },
  getters: {
    [types.GET_USER] (state) {
      return state.user
    }
  },
  mutations: {
    [types.SET_USER] (state, payload) {
      state.user = payload
    }
  }
}
export default authModule
