import api from '../../utils/api'
import * as types from '../mutation-types'
export default {
  state: {
    currentPost: false,
    posts: {},
    post: {},
    categories: []
  },

  actions: {

    // All Posts
    async [types.FETCH_POSTS] ({ commit }) {
      try {
        var res = await api.get('/admin/api/posts')
        commit(types.FETCH_POSTS, res.data.posts)
        //commit(types.FETCH_CATEGORIES, res.data.categories)
      } catch (e) {
         throw e
      }
    },

    async [types.CREATE_POST] ({ commit }, post) {
      try {
        await api.post('/admin/api/post/create', post)
      } catch (e) {
        throw e
      }
    },

    // Update a post
    async [types.UPDATE_POST] ({ commit }, post) {
      try {
        var rest = await api.put(`/admin/api/post/update/${post.id}`, post)
        if (rest.data.updated) {
          //Todo: Not sure we need to update the post in the state because we load them again new

        }
      } catch (e) {
        throw e
      }
    },
    // Single Post
    async [types.FETCH_POST] ({ commit }, slug) {
      try {
        var res = await api.get(`/admin/api/post/${slug}`)
        commit(types.FETCH_CATEGORIES, res.data.categories)
        commit(types.FETCH_POST, res.data.post[ 0 ])
      } catch (e) {
        console.error('Error while fetching post', e)
      }
    },
    // Deletes Single Post
    async [types.DELETE_POST] ({ commit, state }, index) {
      var id = state.posts[ index ].id
      try {
        // Todo: do a check here
        var res = await api.delete(`/admin/api/post/delete/${id}`)
        if (res.data.deleted) {
          commit(types.DELETE_POST, index)
        }
      } catch (e) {
        throw e
      }
    },
    // Loads all categorties from the server
    async [types.FETCH_CATEGORIES] ({ commit, state }, index) {
      try {
        var res = await api.get('/admin/api/categories')
        commit(types.FETCH_CATEGORIES, res.data)
      } catch (e) {
        console.log('cant fetch categories', e)
      }
    },
    async [types.CREATE_CATEGORY] ({ commit }, category) {
      try {
        var res = await api.post('/admin/api/category/create', category)
        if (res.data.success) {
          commit(types.ADD_CATEGORY, res.data.category)
        }
      } catch (e) {
        console.log('Failed to create category', e)
      }
    },

    async [types.DELETE_CATEGORY] ({ commit, state }, index) {
      try {
        var res = await api.delete(`/admin/api/category/delete/${state.categories[ index ].id}`)
        if (res.data.deleted) {
          commit(types.DELETE_CATEGORY, index)
        }
      } catch (e) {}
    }
  },

  mutations: {
    // Sets the Post index
    [types.FETCH_POSTS] (state, payload) {
      state.posts = payload
    },
    // Sets the Cateogires
    [types.FETCH_CATEGORIES] (state, payload) {
      state.categories = payload
    },

    [types.ADD_CATEGORY] (state, payload) {
      state.categories.push(payload)
    },

    [types.DELETE_CATEGORY] (state, payload) {
      state.categories.splice(payload, 1)
    },
    // FOr editing a single post
    [types.FETCH_POST] (state, payload) {
      state.post = payload
    },
    // Delete a Post from any where
    [types.DELETE_POST] (state, payload) {
      state.posts.splice(payload, 1)
    }

  },

  getters: {
    [types.GET_CATEGORIES] (state) {
      return state.categories
    },

    [types.GET_POSTS] (state) {
      return state.posts
    },

    [types.GET_POST] (state) {
      return state.post
    }
  }
}
