import api from '../../utils/api'
import * as types from '../mutation-types'
// Todo: commit some Notifications
const mediaModule = {
  state: {
    index: [],
    croppedFile: false,
  },
  actions: {
    async [types.START_UPLOAD] ({ commit }, payload) {
      try {
        var res = await api.put('/admin/api/media/upload', payload)
        if (res.data.upload) {
        }
      } catch (e) {
        throw e
      }
    },
    async [types.FETCH_MEDIA] ({ commit }, dir = '') {
      // Load Media index
      try {
        var res = await api.get(`/admin/api/media/`, {
          params: {
            dir
          }
        })
        commit(types.FETCH_MEDIA, res.data)
      } catch (e) {
        throw e
      }
    },
    async [types.CROP_IMAGE] ({ commit }, payload) {
      try {
        var res = await api.post('/admin/api/media/crop', payload)
        if (res.data.success) {
          commit(types.CROP_IMAGE, res.data.file)
        }
      } catch (e) {
        throw e
      }
    }
  },
  getters: {
    [types.GET_MEDIA] (state) {
      return state.index
    },
    [types.GET_CROPPED_IMAGE] (state) {
      return state.croppedFile
    }
  },
  mutations: {
    [types.CROP_IMAGE] (state, payload) {
      state.croppedFile = payload
    },
    [types.FETCH_MEDIA] (state, payload) {
      state.index = payload
    }
  }
}
export default mediaModule
