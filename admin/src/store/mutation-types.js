// Auth
export const SET_USER = 'auth/SET_USER'
export const LOCAL_LOGIN = 'auth/LOCAL_LOGIN'
export const LOGIN = 'auth/LOGIN'
export const LOGOUT = 'auth/LOGOUT'
export const GET_USER = 'auth/GET_USER'

// Posts
export const FETCH_POSTS = 'blog/FETCH_POSTS'
export const GET_POSTS = 'blog/GET_POSTS'

// Single Post
export const FETCH_POST = 'blog/FETCH_POST'
export const GET_POST = 'blog/GET_POST'

// Post
export const CREATE_POST = 'blog/CREATE_POST'
export const UPDATE_POST = 'blog/UPDATE_POST'
export const DELETE_POST = 'blog/DELETE_POST'

// Blog
export const FETCH_CATEGORIES = 'blog/FETCH_CATEGORIES'
export const GET_CATEGORIES = 'blog/GET_CATEGORIES'
export const DELETE_CATEGORY = 'blog/DELETE_CATEGORY'
export const ADD_CATEGORY = 'blog/ADD_CATEGORY'
export const CREATE_CATEGORY = 'blog/CREATE_CATEGORY'

// Upload

export const START_UPLOAD = 'upload/START_UPLOAD'
export const UPLOAD_FINISHED = 'upload/UPLOAD_FINISHED'
export const UPLOAD_STATUS = 'upload/UPLOAD_STATUS'

// Media
export const FETCH_MEDIA = 'media/FETCH_MEDIA'
export const GET_MEDIA = 'media/GET_MEDIA'
export const ADD_MEDIA = 'media/ADD_MEDIA'

export const RENAME_MEDIA = 'media/RENAME_MEDIA'
export const DELETE_MEDIA = 'media/DELETE_MEDIA'

export const CROP_IMAGE = 'media/CROP_IMAGE'
export const STOP_CROPPING = 'media/STOP_CROPPING'
export const GET_CROPPED_IMAGE = 'media/GET_CROPPED_IMAGE'
