import axios from 'axios'
import checkAuth from './auth'
let token = checkAuth()

axios.interceptors.request.use(function (config) {
  if (token) {
    config.headers['Authorization'] = `Bearer ${token.originalToken}`
  }
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

axios.interceptors.response.use(response => {
  return response
}, err => {
  if (err.response.status === 403 || err.response.status === 401) {
    window.localStorage.removeItem('token')
    window.location.href = '/admin/login'
  }
  // localStorage.removeItem('token')
  return Promise.reject(err)
})

export default axios
