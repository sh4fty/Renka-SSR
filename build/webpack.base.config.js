const path = require('path')
const webpack = require('webpack')
const vueConfig = require('./vue-loader.config')
module.exports = {
  devtool: '#source-map',
  entry: {
    app: './web/client-entry.js',
    vendor: [ 'vue', 'vue-router', 'vuex', 'lru-cache', 'es6-promise' ]
  },
  output: {
    path: path.resolve(__dirname, '../dist/'),
    publicPath: '/dist/',
    filename: 'client-bundle.js'
  },

  resolve: {
    alias: {
      modernizr$: path.resolve(__dirname, './.modernizrrc')
    }
  },

  resolveLoader: {
    //root: path.join(__dirname, '../node_modules')
  },
  module: {
    rules: [
      { test: /\.json$/, loader: 'json-loader' },
      {
        test: /\.vue$/,
        loader: 'vue'
      }, {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url'
      }, {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url'
      },
      {
        test: /\.modernizrrc$/,
        loader: 'modernizr'
      },
      {
        test: /\.s[a|c]ss$/,
        loader: 'style!css!sass'
      }
    ],
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      vue: vueConfig
    })
  ]
}
