const base = require('./webpack.base.config')
const webpack = require('webpack')
const vueConfig = require('./vue-loader.config')
var CompressionPlugin = require("compression-webpack-plugin");
const BabiliPlugin = require("babili-webpack-plugin");

const config = Object.assign({}, base, {
  plugins: base.plugins.concat([
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'client-vendor-bundle.js'
    })
  ])
})

if (process.env.NODE_ENV === 'production') {
  console.log('IS IN PRODUCTIOn')
  const ExtractTextPlugin = require('extract-text-webpack-plugin')
/*
  config.vue.loaders = {
    sass: ExtractTextPlugin.extract({
      loader: 'css-loader!sass-loader',
      fallbackLoader: 'vue-style-loader'
    })
  }*/
  vueConfig.loaders = {
    sass: ExtractTextPlugin.extract({
      loader: "css-loader!sass-loader",
      fallbackLoader: "vue-style-loader" // <- this is a dep of vue-loader
    })
  }
  config.plugins.push(
    new ExtractTextPlugin('styles.css'),
   /* new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }), */

    new BabiliPlugin(),
    new CompressionPlugin({
      asset: "[path].gz[query]",
      algorithm: "gzip",
      test: /\.js$|\.css$|/,
      threshold: 10240,
      minRatio: 0.8
    })
  )
}

module.exports = config
