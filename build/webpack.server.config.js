const webpack = require('webpack')
const base = require('./webpack.base.config')

/*
 You just need to inject the appropriate og tags into the HTML before dispatching it to the requesting browser. I had to double check, but that's really all Facebook needs. If you need to use your store's logic on the server side to generate those og tags, just create a Vue component which you use only for SSR. You can use the renderer's "renderToString" instead of "renderToStream", just to produce an accurate HTML head. You can still stream the app render.
 In webpack-speak, you'd create 3 entries.. 1 for client-side app, 1 for server-side HTML head, and 1 for server-side app. You are not forced to provide the HTML head component on the client-side.
 This works because on the server side, you can provide the same context object to both the HTML head and the app.
 */
module.exports = Object.assign({}, base, {
  target: 'node',
  devtool: false,
  entry: './web/server-entry.js',
  output: Object.assign({}, base.output, {
    filename: 'server-bundle.js',
    libraryTarget: 'commonjs2'
  }),
  externals: Object.keys(require('../package.json').dependencies),
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'process.env.VUE_ENV': '"server"'
    })

  ]
})
