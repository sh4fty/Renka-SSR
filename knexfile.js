// Update with your config settings.

module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: __dirname + '/server/db/dev.sqlite3'
    },
    migrations: {
      directory: __dirname + '/server/db/migrations',
      tableName: 'knex_migrations'
    },
    seeds: {
      directory:  __dirname +  '/server/db/seeds'
    },
    useNullAsDefault: true
  },
  production: {
    client: 'sqlite3',
    connection: {
      filename: './server/db/prod.sqlite3'
    },
    useNullAsDefault: true,

    migrations: {
      directory: __dirname + '/server/db/migrations',
      tableName: 'knex_migrations'
    }
  }

};
