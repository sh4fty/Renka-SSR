const PostController = require('../api/modules/Post/PostController')
const CategoryController = require('../api/modules/Category/CategoryController')
const MediaController = require('../api/modules/MediaManager/MediaController')
const router = require('express').Router()
const uploadHandler = require('../middleware/uploadHandler')
/*
 get: admin/api/posts
 */
router.get('/posts', PostController.index)
router.put('/post/update/:id', PostController.updateById)

router.get('/post/:slug?', PostController.singlePostExtended)
router.delete('/post/delete/:id', PostController.destroy)

/**
 * get: admin/api/post/create/:id?grgfefefefefef
 */
// router.get('/post/:id?', PostController.createOrEditView)
router.post('/post/create', PostController.create)
router.delete('/post/:id', PostController.destroy)

router.get('/categories', CategoryController.index)
router.post('/category/create', CategoryController.create)
router.get('/media/:dir?', MediaController.index)
router.post('/media/crop', MediaController.cropMedia)

router.put('/media/upload', uploadHandler, MediaController.uploadMedia)

router.delete('/category/delete/:id', CategoryController.remove)
module.exports = router
