const knex = require('../../db/config')
const objection = require('objection')
const ValidationError = require('objection').ValidationError
const Model = objection.Model
Model.knex(knex)
class BaseModel extends Model {

  $beforeSave (inserting) {
    return this.constructor.query().select('id').where('slug', this.slug).first().then(row => {
      if (typeof row === 'object' && row.id) {
        if (inserting || Number.parseInt(row.id) !== Number.parseInt(this.id)) {
          throw new ValidationError({
            slug: `Slug is already in use by #${row.id}`
          })
        }
      }
    })
  }
}

module.exports = BaseModel;
