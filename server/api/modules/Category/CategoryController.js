const Category = require('./CategoryModel')

const debug = require('debug')('renka:CategoryController')

function index (req, res, next) {
  Category.query().then((result) => {
    res.status(200).json(result)
  }).catch(e => {
    res.status(500).json({ error: e })
  })
}

function remove (req, res, next) {
  Category
    .query()
    .deleteById(req.params.id)
    .then(() => {
      res.status(200).json({ deleted: true })
    })
    .catch(e => {
      res.status(500).json({ error: e })
    })
}

// Creates a  Category Admin route
function create (req, res, next) {
  Category.query().insert({
    name: req.body.name,
    is_public: true
  })
    .then((category) => {
      res.status(200).json({ success: true, category })
    })
    .catch(e => {
      res.status(500).json(e)
    })
}

/**
 * @description get /api/:slug/posts
 * @scope public
 * @param params
 * @param res
 * @param next
 */
function categoryPosts ({ params }, res, next) {
  console.log(params)
  Category.query()
    .where('slug', '=', params.slug)
    .eager('posts.user')
    .omit([ 'password', 'email' ])
    .then((categoryWithPosts) => {
      res.status(200).json(categoryWithPosts)
    })
    .catch((e) => {
      //Todo: Error händling
    })
}

module.exports = { remove, index, create, categoryPosts }
