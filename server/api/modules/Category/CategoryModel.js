const BaseModel = require('../BaseModel')
const slug = require('slug')
const join = require('path').join
class Category extends BaseModel {
  static get tableName () {
    return 'Category'
  }
  $beforeInsert () {
    this.created_at = new Date().toISOString()
    this.updated_at = new Date().toISOString()
    this.slug = slug(this.name.toLowerCase())

    return this.$beforeSave(true)

  }
  $beforeUpdate () {
    this.updated_at = new Date().toISOString()
    this.slug = slug(this.name.toLowerCase())
  }

  static get relationMappings () {
    return {
      posts: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: join(__dirname, '..', 'Post', 'PostModel.js'),
        join: {
          from: 'Category.id',
          through: {
            from: 'Post_Category.category_id',
            to: 'Post_Category.post_id'
          },
          to: 'Post.id'
        }
      }
    }
  }
}
module.exports = Category
