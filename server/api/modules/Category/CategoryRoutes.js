const router = require('express').Router()
const Controller = require('./CategoryController')

router.get('/:slug/posts', Controller.categoryPosts)


module.exports = router