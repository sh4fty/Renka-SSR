const path = require('path')
const uploadDir = path.join(__dirname, '/..', '/..', '/..', '/uploads/')
//const Media = require('./MediaModel')
const fileManager = require('../../../utils/FileManager')
const jimp = require('jimp')
/*
 table.increments()
 table.string('path').notNullable().unique()
 table.string('type')
 table.string('thumbnail').notNullable().unique()
 table.timestamps()
 */

/**
 * @description gets the whole dir with files
 * @param req
 * @param res
 * @param next
 */
function index (req, res, next) {
  var dir = req.query.dir || '/'
  fileManager.listDir(dir).then(result => {
    res.status(200).json(result)
  }).catch(e => {
    res.status(404).json({ error: e })
  })
}
function deleteMedia (req, res, next) {
}

function uploadMedia (req, res, next) {
  var createThumbnails = function (images) {
    return new Promise((resolve, reject) => {
      var i = 0
      for (let image of images) {
        console.log('Start image', ++i)
        image.resize(256, 256)
          .quality(60)
          .write(path.join(uploadDir, '/thumbnails/', `thumb_${image.name}`))
      }
      resolve()
    })
  }
  Promise.all(req.files.map(file => {
    return jimp.read(file.path)
  })).then(images => {
    // Todo: replace dots in images names
    for (let image in req.files) {
      images[ image ].name = req.files[ image ].path.split('\\').pop()
    }
    return createThumbnails(images)
  }).then(() => {
    res.status(200).json({ upload: true })
  }).catch(e => {
    res.status(500).json({ uploadError: e })
  })
}

function cropMedia (req, res, next) {
  // image.crop( x, y, w, h );
  var imagePath = path.join(uploadDir, req.body.filePath)
  var fileName = path.parse(imagePath).base
  jimp.read(imagePath).then((image) => {
    image.crop(req.body.x, req.body.y, req.body.width, req.body.height)
      .write(path.join(uploadDir, 'crop', fileName))
    res.status(200).json({ success: true, file: path.join('crop', fileName) })
  })
    .catch(e => {
      res.status(500).json({ error: e })
    })

  /* fileManager().then(result => {

   }).catch(e => {
   res.status(500).json({ error: e })
   }) */
}

module.exports = {
  uploadMedia,
  cropMedia,
  deleteMedia,
  index
}
