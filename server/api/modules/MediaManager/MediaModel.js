const BaseModel = require('../BaseModel')

class Media extends BaseModel {
  static get tableName () {
    return 'Media'
  }

  $beforeInsert () {
    this.created_at = new Date().toISOString()
    this.updated_at = new Date().toISOString()

    return this.$beforeSave(true)
  }

  $beforeUpdate () {
    this.updated_at = new Date().toISOString()
  }
}
module.exports = Media
