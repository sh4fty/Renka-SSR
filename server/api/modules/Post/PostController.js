const debug = require('debug')('renka:PostController')
const Post = require('./PostModel')
const Category = require('../Category/CategoryModel')

/**
 * This is the main api for Vue front
 * @param req
 * @param res
 * @param next
 */
function index (req, res, next) {
  var categories = Category.query().where('is_public', '=', true)
  var posts = Post.query()
    .eager('[user,categories]')
    .omit([ 'email', 'password', 'is_public', 'updated_at' ])
    .orderBy('created_at', 'desc')
  Promise.all([ posts, categories ]).then(([postResults, categoriesResults]) => {
    res.status(200).json({
      posts: postResults,
      categories: categoriesResults
    }).catch(e => {
      console.log(e)
    })
  })
}

function getOnlyPostIndex (req, res, next) {
  Post.query()
    .eager('[user,categories]')
    .omit([ 'email', 'password', 'updated_at' ])
    .orderBy('created_at', 'desc')
    .then((posts) => {
      res.status(200).json({
        posts: posts
      })
    }).catch(e => {
    // Todo: Error handler
  })
}

function singlePost ({ params }, res, next) {
  Post.query()
    .where('slug', '=', params.slug)
    .eager('[user,categories]')
    .omit([ 'password', 'email', 'excerpt', 'content_mark' ])
    .first()
    .then((post) => {
      res.status(200).json(post)
    })
    .catch(e => {
      res.status(500).json({ error: e })
    })
}

function updateById (req, res, next) {

  var postID = req.params.id
  // console.log(req.body)

  var updatePost = Post.query()
    .where('id', postID).first()
    .then(post => post.$relatedQuery('categories').unrelate())
    .then(() => {
      return Post.query().patchAndFetchById(postID, req.body).then((post) => {
        if (req.body.categories) {
          return Promise.all(req.body.categories.map((category) => {
            return post.$relatedQuery('categories').relate({ id: category.id })
          }))
        } else {
          Promise.resolve(true)
        }
      })
    })
  updatePost.then(() => {
    res.status(200).json({ updated: true })
  }).catch(e => {
    res.status(500).json({error: e})
  })
}

function destroy (req, res, next) {
  var deletePost = Post.query()
    .where('id', req.params.id)
    .first()
    .then(post => post.$relatedQuery('categories').unrelate())
    .then(() => {
      return Post.query()
        .where('id', req.params.id)
        .first()
        .delete()
    })
  deletePost.then((post) => {
    res.status(200).json({ deleted: true })
  }).catch(e => {
    res.status(500).json({ error: e })
  })
}

function create (req, res, next) {
  Post.query()
    .insertAndFetch(req.body)
    .then((post) => {
      if (req.body.categories) {
        return Promise.all(req.body.categories.map((category) => {
          return post.$relatedQuery('categories').relate({ id: category.id })
        }))
      } else {
        Promise.resolve(true)
      }
    })
    .then(success => {
      res.status(200).json(success) // Todo: Not sure this right
    })
    .catch(err => {
      res.status(500).json({ error: err })
    })
}

/**
 * @return {object} Return the current post with all Categories and mark the used ones as selected
 * @param req
 * @param res
 * @param next
 */
function singlePostExtended (req, res, next) {
  var promises = []
  var categories = []
  if (req.params.slug) {
    // Todo: We could use ID
    promises.push(Post.query().where('slug', '=', req.params.slug).eager('[categories]'))
  }
  promises.push(Category.query()) // This can be empty
  Promise.all(promises).then(([postQueryResult, categoryQueryResult]) => {
    // if(!postQueryResult[0] instanceof Post)
    if (categoryQueryResult) {
      const findCategoryInPost = function (categoryId) {
        for (let category of postQueryResult[ 0 ].categories) {
          if (categoryId === category.id) return true
        }
      }
      for (let category of categoryQueryResult) {
        category.select = findCategoryInPost(category.id) === true
        categories.push(category)
      }
    }
    res.status(200).json({ post: postQueryResult, categories })
  }).catch(e => {
    res.status(500).json({ error: e })
  })
}

module.exports = { singlePostExtended, updateById, index, getOnlyPostIndex, create, destroy, singlePost }
