const BaseModel = require('../BaseModel')

const join = require('path').join
const slug = require('slug')

class Post extends BaseModel {
  static get tableName () {
    return 'Post'
  }

  static get relationMappings () {
    return {
      user: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: join(__dirname, '..', 'User', 'UserModel.js'),

        join: {
          from: 'User.id',
          to: 'Post.user_id'
        }
      },
      categories: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: join(__dirname, '..', 'Category', 'CategoryModel.js'),
        join: {
          from: 'Post.id',
          through: {
            from: 'Post_Category.post_id',
            to: 'Post_Category.category_id'
          },
          to: 'Category.id'
        }
      }
    }
  }

  $beforeInsert () {
    console.log(this)
    this.created_at = new Date().toISOString()
    this.updated_at = new Date().toISOString()
    this.slug = slug(this.title.toLowerCase())
  }

  $beforeUpdate () {
    this.updated_at = new Date().toISOString()

    this.slug = slug(this.title.toLowerCase())
  }
}

module.exports = Post
