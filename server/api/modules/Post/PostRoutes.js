const router = require('express').Router()
const PostController = require('./PostController')

/**
 * get /api/post/index
 */

router.get('/post/:slug', PostController.singlePost)
router.get('/', PostController.index)
router.get('/posts/:slug?', PostController.getOnlyPostIndex)
//router.post('/index', Controller.indexView)

module.exports = router
