const BaseModel = require('../BaseModel')
const passwordHash = require('password-hash')
class User extends BaseModel {

  static get tableName () {
    return 'User'
  }

  /**
   * Hooks
   */
  $beforeInsert () {

    this.password = passwordHash.generate(this.password, {
      iterations: 9
    })

    this.created_at = new Date().toISOString()
    this.updated_at = new Date().toISOString()
  }

  $beforeUpdate () {
    this.updated_at = new Date().toISOString()
  }
}

module.exports = User
