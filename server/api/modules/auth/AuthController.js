const User = require('../User/UserModel')
const hashedPassword = require('password-hash')
const jwt = require('jsonwebtoken')
function login (req, res, next) {
  if (!req.body.password || !req.body.email) {
    return res.status(401).json('Failed')
  }
  User.query().where('email', '=', req.body.email).then(user => {
    if (!user[ 0 ] instanceof User) {
      res.status(401).json('Failed')
    }
    var token
    if (hashedPassword.verify(req.body.password, user[ 0 ].password)) {
      token = jwt.sign({
        user: {
          userId: user[ 0 ].id,
          username: user[ 0 ].username
        }
      }, process.env.JWT, {
        expiresIn: '2 days'
      })
      if (token) {
        res.status(200).json({ token })
      } else {
        res.status(401).json('Failed')
      }
    } else {
      res.status(401).json('Failed')
    }
  }, err => {
    res.status(401).json('Failed', { error: err })
  })
}
module.exports = {
  login
}
