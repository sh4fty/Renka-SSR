const router = require('express').Router()

router.use('/blog', require('./modules/Post/PostRoutes'))
router.use('/category', require('./modules/Category/CategoryRoutes'))
module.exports = router
