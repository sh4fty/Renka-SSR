const passport = require('passport')
const LocalStrategy = require('passport-local')

const hashedPassword = require('password-hash')
const User = require('../api/modules/User/UserModel')

passport.serializeUser(function (user, done) {
  done(null, user.id)
})

passport.deserializeUser(function (id, done) {
  User.query().where({
    id: id
  }).then((user) => {
    done(null, user[ 0 ])
  })
})

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, function (email, password, done) {
  User.query().where({
    email: email
  }).then((user) => {
    if (hashedPassword.verify(password, user[ 0 ].password)) {
      return done(null, user[ 0 ]);
    } else {
      return done(null, false, { message: 'Not for you' })
    }
  }).catch(() => {
    return done(null, false, { message: 'Not for you' })
  })
}))
module.exports = passport
