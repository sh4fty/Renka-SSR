const config = require('../../knexfile')
const Knex = require('knex')
const env = process.env.NODE_ENV === 'production' ? 'production' : 'development'
const knex = Knex(config[env])
module.exports = knex
