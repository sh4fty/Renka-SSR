exports.up = function (knex, Promise) {
  return Promise.all([ knex.schema.createTableIfNotExists('Post', function (table) {
    table.increments()
    table.string('title')
    table.string('slug')
    table.text('content_mark')
    table.text('content_html')
    table.integer('user_id')
    table.timestamps()
  }),
    knex.schema.createTableIfNotExists('Post_Category', function (table) {
      table.increments('id').primary()
      table.integer('post_id').notNullable().references('id').inTable('Post').onDelete('CASCADE')
      table.integer('category_id').notNullable().references('id').inTable('Category').onDelete('CASCADE')

    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([ knex.schema.dropTable('Post'), knex.schema.dropTable('Post_Category') ])
}
