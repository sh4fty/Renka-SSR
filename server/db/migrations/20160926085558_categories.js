exports.up = function (knex, Promise) {
  return Promise.all([ knex.schema.createTableIfNotExists('Category', function (table) {
    table.increments()
    table.string('name').notNullable().unique()
    table.string('slug').notNullable().unique()
    table.boolean('is_public')
    table.timestamps()
  })
  ])
}

exports.down = function (knex, Promise) {
 return Promise.all([knex.schema.dropTable('Category')])
}
