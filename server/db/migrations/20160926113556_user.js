exports.up = function (knex, Promise) {
  return Promise.all([ knex.schema.createTableIfNotExists('User', function (table) {
    table.increments('id')
    table.string('email').unique().notNullable()
    table.string('username')
    table.string('first_name')
    table.string('last_name')
    table.string('password').unique().notNullable()
    table.timestamps()
  })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([ knex.schema.dropTable('User') ])
}
