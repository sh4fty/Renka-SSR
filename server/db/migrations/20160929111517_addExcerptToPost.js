exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('Post', function (table) {
      table.string('excerpt')
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('Post', function (table) {
      table.dropColumn('excerpt')
    })
  ])
}