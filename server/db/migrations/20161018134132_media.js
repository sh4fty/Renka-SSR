exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.createTableIfNotExists('Media', function (table) {
      table.increments()
      table.string('path').notNullable().unique()
      table.string('type')
      table.string('thumbnail').notNullable().unique()
      table.timestamps()
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([ knex.schema.dropTable('Media') ])
}
