exports.up = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('Media', function (table) {
      table.string('size')
    })
  ])
}

exports.down = function (knex, Promise) {
  return Promise.all([
    knex.schema.table('Media', function (table) {
      table.dropColumn('size')
    })
  ])
}