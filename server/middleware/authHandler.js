var jwt = require('jsonwebtoken')

module.exports = function (req, res, next) {
  const authorizationHeader = req.headers[ 'authorization' ]
  let token
  if (authorizationHeader) {
    token = authorizationHeader.split(' ')[ 1 ]
  }

  if (token) {
    jwt.verify(token, process.env.JWT, (err, decoded) => {
      if (err) {
        res.status(401).json({ error: 'Failed to auth' })
      } else {
        // Todo: we dont need to check the user exists for now

        req.user = decoded.user
        next()
      }
    })
  } else {
    res.status(403).json({ error: 'No Token provided' })
  }
}
