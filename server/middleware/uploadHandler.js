const formidable = require('formidable')
const path = require('path')
const uploadDir = path.join(__dirname, '..', '/uploads/')
const sanitize = require('safename')

module.exports = function uploadHandler (req, res, next) {
  var form = new formidable.IncomingForm()
  var files = []
  form.multiples = true
  form.keepExtensions = true
  form.uploadDir = uploadDir
  form.parse(req, (err, fields, files) => {
    if (err) return res.status(500).json({ error: err })
  })

  form.on('fileBegin', function (name, file) {
    var sanitziedFileName = sanitize(file.name, '_')
    var fileInfo = path.parse(sanitziedFileName)
    file.path = path.join(uploadDir, `${fileInfo.name}_${new Date().getTime()}${fileInfo.ext}`)
  })

  form.on('file', function (field, file) {
    files.push(file)
  })

  form.on('end', () => {
    req.files = files
    next()
  })
}
