process.env.VUE_ENV = 'server'
const isProd = process.env.NODE_ENV === 'production'
require('dotenv').config()
var exphbs = require('express-handlebars')
const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const path = require('path')
var moment = require('moment')
const favicon = require('serve-favicon')
const serialize = require('serialize-javascript')
const ensureAuth = require('./middleware/authHandler')

moment.locale('de');
const resolve = file => path.resolve(__dirname, file)
const createBundleRenderer = require('vue-server-renderer').createBundleRenderer
const app = express()
/**
 * Parsers
 */
app.use(bodyParser.json())
// Todo: if dir not exists create them for uploads
/**
 * Views
 */
app.set('views', path.join(__dirname, 'views'))
var hbs = exphbs.create({
  layoutsDir: path.join(__dirname, 'views/layouts'),
  partialsDir: path.join(__dirname, 'views/partials'),
  extname: 'hbs',
  helpers: {
    date: (date) => {
      return moment(date).format('MMMM Do YYYY, HH:mm:ss')
    }
  }
})
app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')

/**
 * Generate Bundle
 */
let renderer
if (isProd) {
  // create server renderer from real fs
  const bundlePath = resolve('../dist/server-bundle.js')
  renderer = createRenderer(fs.readFileSync(bundlePath, 'utf-8'))
} else {
  require('../build/setup-dev-server')(app, bundle => {
    renderer = createRenderer(bundle)
  })
}

function createRenderer (bundle) {
  return createBundleRenderer(bundle, {
    cache: require('lru-cache')({
      max: 1000,
      maxAge: 1000 * 60 * 15
    })
  })
}
/**
 * Routes
 */
app.use('/dist', express.static(resolve('../dist')))
app.use('/uploads', express.static(resolve('./uploads')))

app.use('/', express.static('../static'))

var adminRoutes = require('./api/adminRoutes')

app.use('/admin/api/', ensureAuth, adminRoutes)

// app.use('/vendor', express.statgregergergic(resolve('../adminVendor')))
// app.use(favicon(resolve('./src/assets/logo.png')))

const apiRoutes = require('./api/routes')
app.use('/api', apiRoutes) // Public api
const publicRoutes = require('./api/publicRoutes')
app.use('/', publicRoutes)

app.use(function (req, res, next) {
  res.locals.user = req.user
  next()
})

app.get('*', (req, res) => {
  if (!renderer) {
    return res.end('waiting for compilation...')
  }

  const context = { url: req.url }
  const renderStream = renderer.renderToStream(context)
  let firstChunk = true
  var templateData = {}
  templateData.style = !!isProd
  renderStream.on('data', chunk => {

    if (firstChunk) {
      if (context.initialState) {
        templateData.bodyClass = context.initialState.route.meta.bodyClass || ''
        templateData.bgColor = context.initialState.route.meta.revealer || ''
        //  templateData.title = context.initialState.Seo.title || 'Renka'
        templateData.state = `window.__INITIAL_STATE__=${serialize(context.initialState, { isJSON: true })}`
      }
      firstChunk = false
    }
    templateData.content = chunk
  })

  renderStream.on('end', () => {
    res.set('Cache-Control', 'no-cache')
    res.render('index', templateData)
  })

  renderStream.on('error', err => {
    throw err
  })
})

/**
 * Fire
 */
const port = process.env.PORT || 8080
app.listen(port, process.env.HOST, () => {
  console.log(`server started at localhost:${port}`)
})
