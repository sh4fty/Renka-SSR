const validator = require('validator')
const isEmpty = require('lodash').isEmpty
/**
 * Validates the Post
 * @param post
 * @returns {*}
 */
module.exports.validatePost = (post) => {
  var errors = {}
  if (validator.isEmpty(post.title)) {
    errors.title = 'Title is required'
  }

  if (validator.isEmpty(post.excerpt)) {
    errors.excerpt = 'Excerpt is required'
  }

  if (post.categories < 1) {
    errors.categories = 'Please enter One Catgory'
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}
