const fs = require('fs-promise')
const path = require('path')
const mainPath = path.join(__dirname, '/..', '/uploads/')
const co = require('co')
function * listDir (mainDir) {
  mainDir = path.join(mainPath, mainDir)
  var output = []
  const getStat = function* (dir) {
    var stat = yield fs.stat(dir)
    // Todo: use the original stat values dont rename
    return {
      folder: stat.isDirectory(),
      size: stat.size,
      ctim: stat.ctime.getTime(),
      mtime: stat.mtime.getTime()
    }
  }
  var dirs = yield fs.readdir(mainDir)
  for (let dir of dirs) {
    var stat = yield getStat(path.join(mainDir, dir))
    output.push({
      name: dir,
      state: stat,
      isFolder: stat.folder
    })
  }
  return output
}

module.exports = {
  listDir: co.wrap(listDir)
}
