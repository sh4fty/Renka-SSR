require('es6-promise/auto')

import Vue from 'vue'
import { sync } from './store/routerStore'

import App from './App.vue'
import store from './store/index'
import router from './router/index'
import moment from 'moment'
// Global Components
import ScramblerComponent from './components/deco/Scrambler.vue'
import WorkPage from './views/Page/Pages/work.vue'
import ContactPage from './views/Page/Pages/contact.vue'

moment.locale('de')

Vue.component('Scrambler', ScramblerComponent)
Vue.component('Work', WorkPage)
Vue.component('Contact', ContactPage)

Vue.filter('debug', function (string) {
  return string + '- debugged'
})

Vue.filter('date', function (date) {
  return moment(date).format('LL')
})

sync(store, router)
const app = new Vue({
  router,
  store,
  ...App
})
export { app, router, store }
