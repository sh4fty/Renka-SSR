require('babel-polyfill')
import { app, store } from './app'
import router from './router/index'
import Vue from 'vue'
import Modernizr from 'modernizr'
import Reaveler from './libs/Revealer'
store.replaceState(window.__INITIAL_STATE__)

const revealerSettings = {
  bgColor: [ '#001bec', '#0015b4', '#00129b' ],
  layers: 3,
  effect: 'anim--effect-3'
}
const revealer = new Reaveler(revealerSettings)

function changeLayout (bodyClass, revealerColors) {
  revealer.changeColors(revealerColors)
  var classList = document.body.classList
  setTimeout(() => {
    if (classList.contains('light')) {
      classList.remove('light')
    } else {
      classList.remove('dark')
    }
    classList.add(bodyClass)
  }, 500)
}

// Just for the first hit
for (let route of router.options.routes) {
  var path = window.location.pathname.split('/')[ 1 ]
  if (route.path.includes(path) && path.length !== 0) {
    var color
    if ('revealer' in route.meta) {
      color = route.meta.revealer[ 0 ]
    } else {
      color = route.meta.background
    }
    document.body.style.background = color
    document.body.classList.add(route.meta.bodyClass)
  }
}

router.beforeEach((route, redirect, next) => {
  if (revealer.isRunning) return false
  var bodyClass = route.meta.bodyClass || false
  if ('revealer' in route.meta) {
    var revealerColors = route.meta.revealer || revealerSettings.bgColor

    setTimeout(() => {
      changeLayout(bodyClass, revealerColors)
    }, 250)
    revealer.reveal('random', 500, (direction) => {
      document.body.style.background = revealer.currentBackground
      next()
    })
  } else {
    document.body.style.background = route.meta.background
    next()
  }
})
app.$mount('#app')
