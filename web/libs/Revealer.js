import utils from 'js-utilities'
import debounce from 'lodash.debounce'
module.exports = class Revealer {
  constructor (settings) {
    this._winsize = { width: window.innerWidth, height: window.innerHeight }
    this._bodyEL = document.body
    this._isRunning = false
    this._effects = [
      'cornerbottomright',
      'cornertopright',
      'cornertopleft',
      'cornerbottomleft',
      'cornerbottomleft',
      'top',
      'right',
      'left',
      'bottom'
    ]

    var config = {
      layers: 3,
      bgColor: [ '#001bec' ],
      effect: 'animation-class',
      onStart: (direction) => {
        return false
      },
      onEnd: (direction) => {
        return false
      }
    }
    this._config = Object.assign(config, settings)
    this._init()
  }

  get isRunning () {
    return this.isAnimating
  }
  /**
   * @description Return the first Backgorund
   * @returns string
   */
  get currentBackground () {
    return this._config.bgColor[ 0 ]
  }
  /**
   * @description Changes the Current Effect colors and generates new divs
   * @param colors
   */
  changeColors (colors) {
    this._config.bgColor = colors
    for (let layer in this.layers) {
      let currentLayer = document.getElementsByClassName('revealer__layer')[ layer ]
      currentLayer.style.background = colors[ layer ]
    }
  }
  /**
   * @description Will initalize
   * @param events
   * @private
   */
  _init (events = true) {
    this._addLayers()
    this.layers = [].slice.call(this.revealerWrapper.children)
    if (events) {
      this._events()
    }
  }

  _addLayers () {
    this.revealerWrapper = document.createElement('div')
    this.revealerWrapper.className = 'revealer'
    this._bodyEL.className += this._config.effect
    var strHTML = ''
    var bgColor = ''
    // when you have more layer as color we use random colors
    for (let i = 0; i < this._config.layers; i++) {
      if (this._config.bgColor[ i ] === undefined) {
        bgColor = this._config.bgColor[ Math.floor(Math.random() * this._config.bgColor.length) ]
      } else {
        bgColor = this._config.bgColor[ i ]
      }
      strHTML += `<div style="background:${bgColor}" class=revealer__layer></div>`
    }
    this.revealerWrapper.innerHTML = strHTML
    this._bodyEL.appendChild(this.revealerWrapper)
  }

  _events () {
    this.debounceResize = debounce(function (ev) {
      this._winsize = { width: window.innerWidth, height: window.innerHeight }
    }, 100)
    window.addEventListener('resize', this.debounceResize)
  }
  /**
   *
   * @param direction
   * @param callbacktime
   * @param callback
   */
  reveal (direction, callbacktime, callback) {
    if (this.isAnimating) {
      return false
    }

    this.isAnimating = true
    if (direction === 'random') {
      direction = this._effects[ Math.floor(Math.random() * this._effects.length) ]
    }
    this._config.onStart(direction)
    var widthVal
    var heightVal
    var transform

    if (direction === 'cornertopleft' || direction === 'cornertopright' || direction === 'cornerbottomleft' || direction === 'cornerbottomright') {
      var pageDiagonal = Math.sqrt(Math.pow(this._winsize.width, 2) + Math.pow(this._winsize.height, 2))
      widthVal = heightVal = pageDiagonal + 'px'
      if (direction === 'cornertopleft') {
        transform = 'translate3d(-50%,-50%,0) rotate3d(0,0,1,135deg) translate3d(0,' + pageDiagonal + 'px,0)'
      } else if (direction === 'cornertopright') {
        transform = 'translate3d(-50%,-50%,0) rotate3d(0,0,1,-135deg) translate3d(0,' + pageDiagonal + 'px,0)'
      } else if (direction === 'cornerbottomleft') {
        transform = 'translate3d(-50%,-50%,0) rotate3d(0,0,1,45deg) translate3d(0,' + pageDiagonal + 'px,0)'
      } else if (direction === 'cornerbottomright') {
        transform = 'translate3d(-50%,-50%,0) rotate3d(0,0,1,-45deg) translate3d(0,' + pageDiagonal + 'px,0)'
      }
    } else if (direction === 'left' || direction === 'right') {
      widthVal = '100vh'
      heightVal = '100vw'
      transform = 'translate3d(-50%,-50%,0) rotate3d(0,0,1,' + (direction === 'left' ? 90 : -90) + 'deg) translate3d(0,100%,0)'
    } else if (direction === 'top' || direction === 'bottom') {
      widthVal = '100vw'
      heightVal = '100vh'
      transform = direction === 'top' ? 'rotate3d(0,0,1,180deg)' : 'none'
    }

    this.revealerWrapper.style.width = widthVal
    this.revealerWrapper.style.height = heightVal
    this.revealerWrapper.style.WebkitTransform = this.revealerWrapper.style.transform = transform
    this.revealerWrapper.style.opacity = 1
    this.revealerWrapper.classList.add(`revealer--${direction}`)
    this.revealerWrapper.classList.add(`revealer--animate`)
    var layersComplete = 0

    this.layers.forEach((layer) => {
      utils.utils.onEndAnimation(layer, () => {
        ++layersComplete
        if (layersComplete === this._config.layers) {
          this.revealerWrapper.classList.remove(`revealer--${direction}`)
          this.revealerWrapper.classList.remove(`revealer--animate`)
          this.revealerWrapper.style.opacity = 0
          this.isAnimating = false
          this._config.onEnd(this.direction)
        }
      })
    })

    if (typeof callback === 'function') {
      clearTimeout(this.callbacktimeout)
      this.callbacktimeout = setTimeout(callback, callbacktime)
    }
  }

  destroy () {
    // Remove the Body class
    this._bodyEL.classList.remove(this._config.effect)
    this._bodyEL.removeChild(this.revealerWrapper)
    this.isAnimating = false
  }
}
