export class Scrambler {
  constructor (el, once = false) {
    if (document.documentElement.classList.contains('touchevents')) {
      return false
    }
    this.isAnimating = false
    this.once = once
    this.el = el
    this.animationsID = false
    this.originalValue = el.innerHTML
    this.chars = [
      '\u2588',
      '\u2593',
      '\u2592',
      '\u2591',
      '\u2588',
      '\u2593',
      '\u2592',
      '\u2591',
      '\u2588',
      '\u2593',
      '\u2592',
      '\u2591',
      '\u003c',
      '\u003e',
      '\u002f'
    ]
  }

  get isRunning () {
    return this.isAnimating
  }

  startScrambling (value = false, newValue = false) {
    if (this.isAnimating) {
      this.stopScrambling()
    }

    this.isAnimating = true
    if (document.documentElement.classList.contains('touchevents')) {
      return false
    }
    var element = this.el
    if (!value) {
      element = value.target
    }

    this.el.style.width = element.offsetWidth + 'px' // prevent change  the width on smaller chars
    this.el.style.height = element.offsetHeight + 'px' // prevent change  the width on smaller chars
    if (!this.once) {
      this._once()
    }
    if (!this.once) {
      this.animationsID = setInterval(() => {
        this._once()
      }, 160)
    } else {
      var maxCount = 5
      var i = 0
      this.animationsID = setInterval(() => {
        if (i++ === maxCount) {
          this.stopScrambling(newValue)
        } else {
          this._once(newValue)
        }
      }, 160)
    }
  }

  _once (originalValue = false) {
    var maxRoundsSolved = 0
    var output = ''
    if (originalValue) {
      this.originalValue = originalValue
    }
    for (let char in this.originalValue) {
      if ((char % 2 || Math.random() >= 0.5) && maxRoundsSolved !== Math.round(this.originalValue.length / 2)) {
        ++maxRoundsSolved
        if (this.originalValue[ char ] !== ' ') {
          output += this._randomChar
        } else {
          output += this.originalValue[ char ]
        }
      } else {
        output += this.originalValue[ char ]
      }
    }
    this.el.innerHTML = output
  }

  stopScrambling (newValue = false) {
    if (!this.animationsID) {
      return
    }

    clearInterval(this.animationsID)
    this.isAnimating = false
    newValue = newValue || this.originalValue
    this.el.innerHTML = newValue
    this.el.style.removeProperty('width')
    this.el.style.removeProperty('height')
  }

  get _randomChar () {
    return this.chars[ Math.floor(Math.random() * this.chars.length) ]
  }
}
