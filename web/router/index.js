import Vue from 'vue'
import Router from 'vue-router'
import BlogView from '../views/Blog/BlogView.vue'
import SinglePost from '../views/Blog/Single.vue'
import BlogList from '../views/Blog/BlogList.vue'

import PageView from '../views/Page/PageView.vue'
import GetStartedView from '../views/Home.vue'

Vue.use(Router)



const blogMeta = {
  bodyClass: 'light',
  revealer: [ '#FFF', '#f4f4f4', '#eaeaea' ]
}

const router = new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (from.path.includes('blog') && to.path.includes('blog')) {
      return savedPosition
    } else {
      return { y: 0 }
    }

  },
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      component: GetStartedView,
      meta: {
        bodyClass: 'dark',
        revealer: [ '#001bec', '#0015b4', '#00129b' ],
      }
    },
    {
      path: '/blog',
      component: BlogView,
      meta: {
        bodyClass: 'light',
        revealer: [ '#FFF', '#f4f4f4', '#eaeaea' ]
      },
      // Blog Children Start
      children: [
        {
          path: '',
          component: BlogList,
          meta: blogMeta
        },

        {
          path: ':slug',
          component: SinglePost,
          meta: {
            bodyClass: 'light',
          }
        },
        {
          path: 'category/:category',
          component: BlogList,
          meta: {
            bodyClass: 'light',
            background: '#fff',
          }
        }
      ]
      // Blog Children End
    },
    {
      path: '/contact',
      component: PageView,
      name: 'Contact',
      meta: {
        bodyClass: 'light',
        revealer: [ '#ffe500', '#FF9500', '#FFAA00' ]
      }
    },
    {
      path: '/work',
      component: PageView,
      name: 'Work',
      meta: {
        bodyClass: 'light',
        revealer: [ '#ffe500', '#FF9500', '#FFAA00' ]
      }
    }
  ]
})
export default router
