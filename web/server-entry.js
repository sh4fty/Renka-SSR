import { app, router, store } from './app'

const isDev = process.env.NODE_ENV !== 'production'

export default context => {
  router.push(context.url)
  const s = isDev && Date.now()
  // Todo: Pass Meta in when preFetch is not set
  /* return Promise.all(router.getMatchedComponents().reduce((accu, component) => {
   if (component.preFetch) {
   return accu.concat(
   [ function () {
   return store.dispatch('setSeoMeta', router.history.current.meta.title)
   } ].concat(component.preFetch).map(function (preFetch) {
   return preFetch.call(preFetch, store)
   })
   )
   }
   return accu


   }, [])).then(() => {
   */
  return Promise.all(router.getMatchedComponents().map(component => {
    if (component && component.preFetch) {
      return component.preFetch(store)
    }
  })).then(() => {

      /* isDev && console.log(`data pre-fetch: ${Date.now() - s}ms`) */
      context.initialState = store.state
      return app
    }
  ).catch(err => {
    console.log(err)
  })
}
