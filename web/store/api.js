import axios from 'axios'
import config from '../config' // Todo: Why i need this whole fucking url

/**
 * FRONT ROUTES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * @type {string}
 */


axios.defaults.baseURL = config.host

/**
 * @param data
 * @returns {*|AxiosPromise}
 */
export function sendContact (data) {
  return axios.post('/api/contact')
}

export function loadPost (slug) {
  return axios.get(`/api/blog/post/${slug}`)
}

export function loadPosts (skip = 0, page = 0) {
  return axios.get(`/api/blog/posts/`)
}

export function fetchPostsByCategory (slug, skip = 0, page = 0) {

  return axios.get(`/api/category/${slug}/posts`)
}

//Todo: Rename and move the api in the Category Controller
export function fetchCategories () {
  return axios.get('/api/blog')
}
