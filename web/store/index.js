require('babel-polyfill')
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import SiteStore from './modules/Site/store'
import Seo from './modules/SEO/store'
import Blog from './modules/Blog'

export default new Vuex.Store({
 // plugins: [ VuexLogger() ],
  modules: {
    SiteStore,
    //  Project,
    Blog,
    Seo
  },
  strict: process.env.NODE_ENV !== 'production'
})
