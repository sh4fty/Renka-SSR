import * as types from '../mutation-types'
import { fetchCategories, loadPost, loadPosts, fetchPostsByCategory } from '../api'
const state = {
  posts: null,
  categories: [],
  post: null,
  currentCategory: false,
}

const actions = {
  getCategories: ({ commit }) => {
    return fetchCategories().then((response) => {
      // commit(types.FETCH_POSTS, response.data.posts)
      commit(types.FETCH_CATEGORIES, response.data.categories)
    }).catch((err) => {
      console.log('Wasted no posts', err)
    })
  },

  /**
   * This Loads the Index Page of Postes mixed
   * @param commit
   * @returns {*}
   */
  fetchInitialPosts: ({ commit }) => {
    return loadPosts().then((response) => {
      commit(types.FETCH_POSTS, response.data.posts)
    })
  },

  singlePost: ({ commit }, slug) => {
    return loadPost(slug).then((response) => {
      commit(types.FETCH_POST, response.data)
    }).catch((err) => {
      console.log('Singlepost failed', err)
    })
  },

  fetchPostsByCategory({ commit }, slug) {
    return fetchPostsByCategory(slug).then((response) => {
      //Todo: Handle Headline via store
      commit(types.FETCH_POSTS, response.data[ 0 ].posts)
    })
  },

}

const getters = {
  getPosts (state) {
    return state.posts
  },

  getPost (state) {
    return state.post
  },

  getCategories (state) {
    return state.categories
  }
}

const mutations = {
  [types.FETCH_POSTS] (state, payload) {
    state.posts = payload
  },
  [types.FETCH_CATEGORIES] (state, payload) {
    state.categories = payload
  },

  [types.FETCH_POST] (state, payload) {
    state.post = payload
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
