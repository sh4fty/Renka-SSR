import * as types from '../mutation-types'
import {} from '../api'
const state = {
  project: 'PROJECT NOT UPDATED'
}

const actions = {
  fetchProject: ({ commit }, projectName) => {
    return fetchProjectFromApi(projectName).then((response) => {
      commit(types.FETCH_PROJECT, response.data)
    }).catch((err) => {
      console.log('Wasted', err)
    })
  }
}

const getters = {
  getProject (state) {
    return state.project
  }
}

const mutations = {
  [types.FETCH_PROJECT] (state, body) {
    state.project = body
  }
}

export default {
  state,
  mutations,
  actions,
  getters
}
