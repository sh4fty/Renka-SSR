import * as types from '../../mutation-types'

// Todo: Rename all this
export const state = {
  title: ''
}

export const actions = {
  setSeoMeta: ({ commit }, title) => {
    commit(types.SEO_CHANGE_TITLE, title)
    return Promise.resolve(title)
  }
}

export const mutations = {
  [types.SEO_CHANGE_TITLE] (state, title) {
    state.title = title
  }
}

export const getters = {
  getSeo (state) {
    return state.title
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}

