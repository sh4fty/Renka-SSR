import * as types from '../../mutation-types'

const state = {
  headline: {
    title: 'Title',
    slogan: 'Slogan'
  }
}

const mutations = {
  [types.SET_HEADLINE] (state, headline) {
    state.headline = headline
  }
}

const actions = {
  setHeadline: ({ commit }, headline) => {
    commit(types.SET_HEADLINE, headline)
  }
}

const getters = {
  getHeadline: (state) => {
    return state.headline
  }
}
export default {
  state,
  mutations,
  actions,
  getters
}
